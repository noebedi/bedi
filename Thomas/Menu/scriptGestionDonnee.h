#ifndef SCRIPTGESTIONDONNEE_H_INCLUDED
#define SCRIPTGESTIONDONNEE_H_INCLUDED

#define PI 3.14159

#define FENETRE_X 1200
#define FENETRE_Y 800

typedef struct
{
    float x = 0;
    float y = 0;
    float mx = 0; //mouvement de l'objet en X par frame
    float my = 0; //mouvement de l'objet en Y par frame
    float taille = 100; //taille en %
    float mtaille = 0; //evolution de la taille de l'objet par frame
    float direction = 0; //en degrès
    float mdirection = 0; //evolution de la direction de l'objet par frame
    float opaciter = 100;
    float mopaciter = 0; //evolution de l'opaciter de l'objets par frame


    int vie = 1;
    int id = 5;

}Objet;

void creeObj(std::vector<Objet> & liste, float posX, float posY) //ajoute un objet à une liste d'objet
{
    Objet obj; //on crée un Objet qui s'ajoutera à une liste d'objet
    obj.x=posX;
    obj.y=posY;

    liste.push_back(obj);
}

void suppObj(std::vector<Objet> & liste, unsigned int position) //supprime un Objet d'une liste d'Objet
{
    if(position>=0 && position<liste.size()) //anti-plantage
    {
        liste[position]=liste.back();
        liste.pop_back(); //supprime la dernière ligne
    }
}


void actualiserObj(std::vector<Objet> & liste) //actualise les paramètres de tous les éléments d'une liste d'objet (position, taille…)
{
    for(unsigned int i=0;i<liste.size();i++) //en C++, on peux initialiser la variable directement dans le for
    {
        liste[i].x+=liste[i].mx;
        liste[i].y+=liste[i].my;
        liste[i].taille+=liste[i].mtaille;
        liste[i].direction+=liste[i].mdirection;


        if(liste[i].vie<=0)
        {
            suppObj(liste,i);
        }
    }
}



void rendu(std::vector<Objet> & liste, sf::RenderWindow & app, sf::Sprite sprite)
{
    for(unsigned int i = 0; i<liste.size();i++)
    {
        sprite.setPosition(liste[i].x,liste[i].y);
        sprite.setScale(liste[i].taille/100,liste[i].taille/100);
        sprite.setRotation(liste[i].direction);

        app.draw(sprite);
    }
}

void actualiserEtRendreObj(Objet & obj,sf::Sprite & sprite,sf::RenderWindow & app)
{
    obj.x+=obj.mx;
    obj.y+=obj.my;
    obj.direction+=obj.mdirection;
    obj.taille+=obj.mtaille;

    sprite.setPosition(obj.x, obj.y);
    sprite.setRotation(obj.direction);
    sprite.setScale(obj.taille/100,obj.taille/100);

    app.draw(sprite);
}

#endif // SCRIPTGESTIONDONNEE_H_INCLUDED

