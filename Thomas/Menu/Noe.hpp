#ifndef NOE_HPP_INCLUDED
#define NOE_HPP_INCLUDED

#include "scriptGestionDonnee.h"
#include "trigo.hpp"
#include <cmath>

void nomFontion(sf::RenderWindow & app, std::vector<Objet> & joueur, sf::Sprite bateau) //modèle fonction
{
    //tous le code
}

void tirvuecote(sf::RenderWindow & app, std::vector<Objet> & joueur,int & numerojoueur,sf::Sprite bateau, sf::Sprite canon, int & scene,float & rotation,sf::Sprite background,sf::Sprite ocean, sf::Sprite ocean2)
{
    while(sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        //on attend que la souris n'est plus presser
    }

    sf::View vueNormal(sf::FloatRect(0,0, FENETRE_X, FENETRE_Y));

    do
    {
        //app.setView(vueNormal);
        sf::Event event; //permet le rage-quit
        while (app.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                app.close();
        }

        bateau.setPosition(-620,650);
        bateau.setScale(1,1);
        bateau.setRotation(0);

        canon.setPosition(bateau.getPosition().x+1240,bateau.getPosition().y-85);
        canon.setScale(1,1);
        canon.setRotation(0);

        ocean.setPosition(0,300);


        sf::Vector2i positionsouris = sf::Mouse::getPosition(app);
        float inclinaison = calculerAngle((positionsouris.x-canon.getPosition().x),(positionsouris.y-canon.getPosition().y));
        inclinaison= -inclinaison*180/3.14159;

        if (inclinaison<-170||inclinaison>90)
            inclinaison= -170;

        else if (inclinaison>-90)
            inclinaison= -90;
        canon.setRotation(inclinaison);

        rotation=-inclinaison+90;


        app.clear();
        app.draw(background);
        app.draw(ocean);
        app.draw(canon);
        app.draw(bateau);
        app.display();

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            scene++;
        }
        app.setFramerateLimit(60);
    }
    while (scene == 4);
}

#endif // NOE_HPP_INCLUDED
