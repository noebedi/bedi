#ifndef SCREENVICTOIRE_H_INCLUDED
#define SCREENVICTOIRE_H_INCLUDED
#include "scriptGestionDonnee.h"
#include "thomas.hpp"

void ScreenVictoir(sf::RenderWindow & app, std::vector<Objet> & joueur,sf::Sprite Screen[],int & scene,sf::Font victoire,sf::Sprite boutonmenu,sf::Sprite boutonrejouer,sf::Sprite boutonquitter, int nbJoueur)
{


        sf::Text texte;
        texte.setFont(victoire);
        char k[50];


        while(true)
        {

        sprintf(k,"VICTOIRE DU\n   JOUEUR %i",joueur[0].id);
        texte.setString(k);
        texte.setCharacterSize(90);
        texte.setColor(sf::Color(255,255,255));
        texte.setPosition(310,100);




        boutonquitter.setPosition(800,600);
        boutonmenu.setPosition(500,600);
        boutonrejouer.setPosition(200,600);



        sf::FloatRect rejouer = boutonrejouer.getGlobalBounds();
        sf::FloatRect menu = boutonmenu.getGlobalBounds();
        sf::FloatRect quitter = boutonquitter.getGlobalBounds();


        if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            sf::Vector2i positionSouris = sf::Mouse::getPosition(app);
            if (rejouer.contains(positionSouris.x, positionSouris.y))
            {
                scene = 2;
                break;

            }
            if (menu.contains(positionSouris.x, positionSouris.y))
            {
                scene = 0;
                break;

            }
            if (quitter.contains(positionSouris.x, positionSouris.y))
            {
                scene = -1;
                break;

            }
        }


        app.draw(Screen[0]);
        app.draw(boutonrejouer);
        app.draw(boutonmenu);
        app.draw(boutonquitter);
        app.draw(texte);

        app.display();

        app.setFramerateLimit(60);

 }
}


#endif // SCREENVICTOIRE_H_INCLUDED


