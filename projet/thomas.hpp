#ifndef THOMAS_HPP_INCLUDED
#define THOMAS_HPP_INCLUDED

#include "mathis.hpp"
#include "aymeric.hpp"
#include <SFML/Audio.hpp>

//Fonction qui applique les textures aux boutons du menu
void appliqueTextureMenu(sf::Sprite bouton[], sf::Texture textBouton[])
{
    char texture[5][50] = { "fond.png", "batoteau.png", "jouer.png", "Options.png", "Quitter.png" };
    for(int i = 0; i < 5; i++)
    {
        if (!textBouton[i].loadFromFile(texture[i]))
        {
            break;
        }
        bouton[i].setTexture(textBouton[i]);
    }
}

//Fonction qui applique les textures aux boutons des options
void appliqueTextureOptions(sf::Sprite bouton[], sf::Texture textJoueurBouton[])
{
    char texture[6][50] = { "2joueur.png", "3joueur.png", "4joueur.png", "fleche.png", "menu.png", "souris.png" };
    for(int i = 0; i < 6; i++)
    {
        if (!textJoueurBouton[i].loadFromFile(texture[i]))
        {
            break;
        }
        bouton[i].setTexture(textJoueurBouton[i]);
    }
}

//Fonction qui place les boutons du menu au bon endroit
void menu(int largeur, int hauteur, sf::RenderWindow &fenetre, sf::Sprite bouton[])
{
    for(int i = 1; i < 5; i++)
    {
        bouton[i].setPosition(largeur/2 - bouton[i].getTexture()->getSize().x/2, hauteur/5 * (i-1) + bouton[i].getTexture()->getSize().y);
        fenetre.draw(bouton[i]);
    }
}

//Fonction qui gere le clique sur les boutons du menu
void gestionMenu(sf::Sprite bouton[], sf::RenderWindow &fenetre, int &scene, int nbJoueur, std::vector<Objet> & joueur)
{
    sf::FloatRect jouer = bouton[2].getGlobalBounds();
    sf::FloatRect options = bouton[3].getGlobalBounds();
    sf::FloatRect quitter = bouton[4].getGlobalBounds();
    sf::Vector2i positionSouris = sf::Mouse::getPosition(fenetre);

    if (jouer.contains(positionSouris.x, positionSouris.y))
        bouton[2].setColor(sf::Color(255,255,255,175));
    else
        bouton[2].setColor(sf::Color(255,255,255,255));

    if (options.contains(positionSouris.x, positionSouris.y))
        bouton[3].setColor(sf::Color(255,255,255,175));
    else
        bouton[3].setColor(sf::Color(255,255,255,255));

    if (quitter.contains(positionSouris.x, positionSouris.y))
        bouton[4].setColor(sf::Color(255,255,255,175));
    else
        bouton[4].setColor(sf::Color(255,255,255,255));

    if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        if (jouer.contains(positionSouris.x, positionSouris.y))
        {
            scene = 2;
            creeJoueur(joueur,nbJoueur);
        }
        if (options.contains(positionSouris.x, positionSouris.y))
        {
            scene = 1;
        }
        if (quitter.contains(positionSouris.x, positionSouris.y))
        {
            scene = -1;
        }
    }
}

//Fonction qui place les boutons des options au bon endroit
void options(int largeur, int hauteur, sf::RenderWindow &fenetre, sf::Sprite bouton[], sf::Font font)
{
    sf::Text joueurr;
    sf::Text commande;
    joueurr.setFont(font);
    joueurr.setString("NOMBRE DE JOUEUR");
    joueurr.setCharacterSize(50);
    joueurr.setPosition(largeur/2 - joueurr.getGlobalBounds().width/2, 0);
    commande.setFont(font);
    commande.setString("COMMANDES :");
    commande.setCharacterSize(50);
    commande.setPosition(0, hauteur/2 + commande.getGlobalBounds().height);
    for(int i = 0; i < 3; i++)
    {
        bouton[i].setPosition(i * (largeur/3) + bouton[i].getTexture()->getSize().x, hauteur/5);
        fenetre.draw(bouton[i]);
    }
    bouton[3].setPosition(0, hauteur/2 + 2*bouton[3].getTexture()->getSize().y/3);
    bouton[4].setPosition(largeur - bouton[4].getTexture()->getSize().x - bouton[4].getTexture()->getSize().x/2, hauteur - 2*bouton[4].getTexture()->getSize().y);
    bouton[5].setPosition(largeur/2 - bouton[5].getTexture()->getSize().x, hauteur - 2*bouton[5].getTexture()->getSize().y);
    fenetre.draw(bouton[3]);
    fenetre.draw(bouton[4]);
    fenetre.draw(bouton[5]);
    fenetre.draw(joueurr);
    fenetre.draw(commande);
}

//Fonction qui gere le clique sur les boutons des options
void gestionOptions(sf::Sprite bouton[], sf::RenderWindow &fenetre, int &scene, int &nbJoueur)
{
    sf::FloatRect twoPlayer = bouton[0].getGlobalBounds();
    sf::FloatRect threePlayer = bouton[1].getGlobalBounds();
    sf::FloatRect fourPlayer = bouton[2].getGlobalBounds();
    sf::FloatRect retourMenu = bouton[4].getGlobalBounds();
    sf::Vector2i positionSouris = sf::Mouse::getPosition(fenetre);

    if (retourMenu.contains(positionSouris.x, positionSouris.y))
        bouton[4].setColor(sf::Color(255,255,255,175));
    else
        bouton[4].setColor(sf::Color(255,255,255,255));

    if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        if (twoPlayer.contains(positionSouris.x, positionSouris.y))
        {
            bouton[0].setColor(sf::Color(0,0,0,255));
            bouton[1].setColor(sf::Color(255,255,255,255));
            bouton[2].setColor(sf::Color(255,255,255,255));
            nbJoueur = 2;
        }
        if (threePlayer.contains(positionSouris.x, positionSouris.y))
        {
            bouton[0].setColor(sf::Color(255,255,255,255));
            bouton[1].setColor(sf::Color(0,0,0,255));
            bouton[2].setColor(sf::Color(255,255,255,255));
            nbJoueur = 3;
        }
        if (fourPlayer.contains(positionSouris.x, positionSouris.y))
        {
            bouton[0].setColor(sf::Color(255,255,255,255));
            bouton[1].setColor(sf::Color(255,255,255,255));
            bouton[2].setColor(sf::Color(0,0,0,255));
            nbJoueur = 4;
        }
        if (retourMenu.contains(positionSouris.x, positionSouris.y))
        {
            scene = 0;
        }
    }
}
void zoom(sf::RenderWindow & app,float taille,Objet joueur,float pourcent)
{

    int x = joueur.x*(pourcent/100);
    int y = joueur.y*(pourcent/100);

    sf::View vueGloable(sf::FloatRect(x,y, FENETRE_X*(taille/10), FENETRE_Y*(taille/10)));
    app.setView(vueGloable);
}
void vueGenerale(std::vector<Objet> joueur, int numJoueur, sf::RenderWindow &fenetre, sf::Sprite bateau, int&scene, sf::Sprite ocean, float lastX, float lastY )
{
    sf::RectangleShape font(sf::Vector2f(FENETRE_X*10,FENETRE_Y*10));
    sf::CircleShape impact(50);
    impact.setOrigin(25,25);
    impact.setFillColor(sf::Color::Red);
    impact.setPosition(lastX,lastY);

    sf::View vueGloable(sf::FloatRect(0,0, FENETRE_X*10, FENETRE_Y*10));
    fenetre.setView(vueGloable);

    font.setFillColor(sf::Color(48,64,61));

    fenetre.clear();
    fenetre.draw(font);
    rendu(joueur,fenetre,bateau);
    fenetre.draw(impact);
    fenetre.display();

    for(int i = 0; i<10; i++) //on affiche les bateaux 5s avec possibilite de fermer la fenetre
    {
        sf::Event event;
        while (fenetre.pollEvent(event))
        {
            // fermer la fenetre
            if (event.type == sf::Event::Closed)
                fenetre.close();
        }

        sf::sleep(sf::milliseconds(100));
    }

    for(float i=0; i<90; i+=1) //on zoom progressivement vers le bateau
    {
        font.setFillColor(sf::Color(48,64,61,255-(2.8*i)));
        zoom(fenetre,(100-i),joueur[numJoueur],i); //on cadre progressivement vers le bateau, mais pas trop pour �viter la triche (pr�-shoot)

        fenetre.clear();
        rendreOcean(fenetre,ocean);
        fenetre.draw(font);
        rendu(joueur,fenetre,bateau);
        fenetre.display();
        fenetre.setFramerateLimit(60);
    }

    scene=3;

}

#endif // THOMAS_HPP_INCLUDED
