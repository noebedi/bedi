#ifndef MATHIS_HPP_INCLUDED
#define MATHIS_HPP_INCLUDED
#include "scriptGestionDonnee.h"
#include "trigo.hpp"
//#include <windows.h>
#include "aymeric.hpp"
#include <cmath>
#define PI 3.14159
#define POSITION_JAUGE 10
#define LARGEUR_JAUGE 30



void deplacerbateau(sf::RenderWindow & app, std::vector<Objet> & joueur,int & numerojoueur,sf::Sprite bateau,sf::Sprite canon, int & scene, sf::Sprite ocean, float & direction, float & canonX, float & canonY, sf::Sprite debrit1, sf::Sprite debrit2, sf::Sprite debrit3, std::vector<Objet> & debritParticule1,std::vector<Objet> & debritParticule2, std::vector<Objet> & debritParticule3)
{
    int taillejauge = 200;
    bool hautBas = false;

    //on initialiser les vues (pour le bateau et l'UI)
    sf::View vueNormal(sf::FloatRect(0,0, FENETRE_X, FENETRE_Y));


    //initialise l'arrière plan de la jauge de carburant
    sf::RectangleShape fondjauge(sf::Vector2f(taillejauge+10, LARGEUR_JAUGE+10));
    fondjauge.setOrigin(5,5);
    fondjauge.setFillColor(sf::Color(100, 100, 100));
    fondjauge.setPosition(POSITION_JAUGE,POSITION_JAUGE);

    //initialise la jauge de carburant
    sf::RectangleShape jauge(sf::Vector2f(taillejauge, LARGEUR_JAUGE));
    jauge.setOrigin(0,0);
    jauge.setFillColor(sf::Color(0, 255, 0));
    jauge.setPosition(POSITION_JAUGE,POSITION_JAUGE);


    do
    {
        app.setView(vueNormal);//on replace la camera normalement pour afficher le reste de l'UI


        sf::Event event;
        while (app.pollEvent(event))
        {
            // Fermer la fenetre
            if (event.type == sf::Event::Closed)
                app.close();
        }


        //change la couleur de la jauge de carburant en fonction du carburant restant
        if (taillejauge > 100)
            jauge.setFillColor(sf::Color(0, 255, 0));
        else if (taillejauge > 75 )
            jauge.setFillColor(sf::Color(255, 255, 0));
        else if (taillejauge > 25 )
            jauge.setFillColor(sf::Color(255, 175, 0));
        else
            jauge.setFillColor(sf::Color(255, 0, 0));



        if(taillejauge>0)
        {
            //commandes pour avancer/reculer
            hautBas=(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down));

            //avancer
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                taillejauge --;
                avancer(joueur[numerojoueur], 5);

            }

            //reculer
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
                taillejauge --;
                avancer(joueur[numerojoueur], -5);
            }

            //tourner à droite
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && hautBas)
            {
                joueur[numerojoueur].direction +=1;
            }

            //tourner à gauche
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && hautBas)
            {
                joueur[numerojoueur].direction -= 1;
            }
        }

        //valider l'angle de tir et passer à la scène suivante
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            scene++;
        }

        if (taillejauge < 0)
            taillejauge =0;

                //met à jour la position du bateau
        bateau.setPosition(joueur[numerojoueur].x,joueur[numerojoueur].y);
        bateau.setScale(joueur[numerojoueur].taille/100,joueur[numerojoueur].taille/100);
        bateau.setRotation(joueur[numerojoueur].direction);

        //met à jour la position et la direction du canon
        float var1 = 125*cos(joueur[numerojoueur].direction*(PI/180));
        float var2 = 125*sin(joueur[numerojoueur].direction*(PI/180));
        canonX=joueur[numerojoueur].x+var1; //canonX et Y sont pointer, on renvoie donc la position du canon par rapport au bateau
        canonY=joueur[numerojoueur].y+var2;
        canon.setPosition(canonX,canonY);
        canon.setScale(0.1,0.1);
        sf::Vector2i positionSouris = sf::Mouse::getPosition(app);
        direction=calculerAngle(positionSouris.x-(var1+600),(var2+400)-positionSouris.y)*180/PI;
        canon.setRotation(direction);



        //on cadre le bateau au centre de la camera
        sf::View vueBateau(sf::FloatRect(joueur[numerojoueur].x-600,joueur[numerojoueur].y-400, 1200, 800));
        app.setView(vueBateau);

        //ajuste la taille de la jauge en fonction des commandes réalisées plus tôt
        jauge.setSize(sf::Vector2f(taillejauge, LARGEUR_JAUGE));

        actualiserObj(debritParticule1);//source dans scriptGestionDonne.h
        actualiserObj(debritParticule2);
        actualiserObj(debritParticule3);

        //dessine les différents éléments
        app.clear();
        rendreOcean(app,ocean);

        rendu(debritParticule1,app, debrit1);
        rendu(debritParticule2,app, debrit2);
        rendu(debritParticule3,app, debrit3);


        app.draw(bateau);
        rendu(joueur,app,bateau);//dessine les autres bateau
        app.draw(canon);

        app.setView(vueNormal);//on replace la camera normalement pour afficher le reste de l'UI

        app.draw(fondjauge);//permet de connaitre le carburant restant
        app.draw(jauge);
        app.display();

        //limite les images par secondes pour éviter que je jeu se déroule trop rapidement
        app.setFramerateLimit(60);

    }
    while (scene == 3 && app.isOpen());

    while(sf::Mouse::isButtonPressed(sf::Mouse::Left))
    {
        //on attend que la souris n'est plus presser
    }

}


#endif // MATHIS_HPP_INCLUDED





