#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>

#include "trigo.hpp"
#include "scriptGestionDonnee.h"
#include "mathis.hpp"
#include "Noe.hpp"
#include "Thomas.hpp"
#include "aymeric.hpp"
#include "ScreenVictoire.h"

#define FENETRE_X 1200
#define FENETRE_Y 800



using namespace sf;

std::vector<Objet> joueur;




int main()
{
    int scene=5, nbJoueur;
    // Create the main window
    RenderWindow app(VideoMode(FENETRE_X, FENETRE_Y), "SFML window");
    app.setFramerateLimit(1);

    // Load a sprite to display
    /*
    Texture textureOcean;
    if (!textureOcean.loadFromFile("ocean2.jpg"))
        return EXIT_FAILURE;
    Sprite ocean(textureOcean);
    */
    
    //charge le background de la victoire
    Texture textureScreenVictoire;
    if (!textureScreenVictoire.loadFromFile("fond Victoire.jpg"))
        return EXIT_FAILURE;
    Sprite ScreenVictoire(textureScreenVictoire);

	//Charge la texture du bateau vu de cote
    Texture textureBateauCote;
    if (!textureBateauCote.loadFromFile("bateau cote.png"))
        return EXIT_FAILURE;
    Sprite bateauCote(textureBateauCote);
    bateauCote.setOrigin(bateauCote.getTexture()->getSize().x / 2, bateauCote.getTexture()->getSize().y / 2);

	//Charge la texture du bateau vu de dessus
    Texture textureBateauDessus;
    if (!textureBateauDessus.loadFromFile("bateau dessus.png"))
        return EXIT_FAILURE;
    Sprite bateauDessus(textureBateauDessus);
    bateauDessus.setOrigin(bateauDessus.getTexture()->getSize().x / 2, bateauDessus.getTexture()->getSize().y / 2);

	//Charge la texture du canon vu de cote
    Texture textureCanonCote;
    if (!textureCanonCote.loadFromFile("canon cote.png"))
        return EXIT_FAILURE;
    Sprite canonCote(textureCanonCote);
    canonCote.setOrigin(canonCote.getTexture()->getSize().x / 2, canonCote.getTexture()->getSize().y / 2);
	//Charge la texture du cano, vu de dessus
    Texture textureCanonDessus;
    if (!textureCanonDessus.loadFromFile("canon dessus.png"))
        return EXIT_FAILURE;
    Sprite canonDessus(textureCanonDessus);
    canonDessus.setOrigin(canonDessus.getTexture()->getSize().x / 2, canonDessus.getTexture()->getSize().y / 2);

	//Charge la texture du missile
    Texture textureMissile;
    if (!textureMissile.loadFromFile("missile.png"))
        return EXIT_FAILURE;
    Sprite missile(textureMissile);
    missile.setOrigin(missile.getTexture()->getSize().x / 2, missile.getTexture()->getSize().y / 2);

	//textures océans
    Texture textureOceanCote;
    if (!textureOceanCote.loadFromFile("ocean cote.png"))
        return EXIT_FAILURE;
    Sprite oceanCote(textureOceanCote);

    Texture textureOceanCote2;
    if (!textureOceanCote2.loadFromFile("ocean cote2.png"))
        return EXIT_FAILURE;
    Sprite oceanCote2(textureOceanCote2);
	//ciel de la scenes 5
        Texture textureScene5;
    if (!textureScene5.loadFromFile("font scene 5.png"))
        return EXIT_FAILURE;
    Sprite scene5(textureScene5);
    
        //pour la scene victoire
	    Font Victoire;
    if (!Victoire.loadFromFile("arial.ttf"))
    {
       return EXIT_FAILURE;
    Text textvictoire;
    textvictoire.setFont(Victoire);
    
        //initialise le menu
    	    
    appliqueTextureMenu(imageMenu, textMenu);
    Texture textOptions[7];
    Sprite imageOptions[7];
    appliqueTextureOptions(imageOptions, textOptions);

    Font font;
    if (!font.loadFromFile("shiny signature.ttf"))
    {
       return EXIT_FAILURE;
    }
    
    Text bruitages[2];
+    bool bruit = false;

    int numerojoueur = 0;
    creeObj(joueur,100,100);


	// //Boucle de la fenetre
    while (app.isOpen())
    {
        Event event;
        while (app.pollEvent(event))
        {
            // fermer la fenetre
            if (event.type == Event::Closed)
                app.close();
        }
        
        
		//deroulement des scenes dans l'ordre

        app.clear();
        if (scene == -1)//quitter
        {
           app.close();
       }
        if(scene==0)//Menu
        {
            app.draw(imageMenu[0]);
            menu(FENETRE_X, FENETRE_Y, app, imageMenu, font, bruitages);
            gestionMenu(imageMenu, app, scene, bruitages, bruit);
        }
        if(scene==1)//Menu
        {
            app.draw(imageMenu[0]);
            options(FENETRE_X, FENETRE_Y, app, imageOptions, font);
            gestionOptions(imageOptions, app, scene, nbJoueur);
        }
        if(scene==2)//Plan d'ensemble
        {
            vueGenerale(joueur, numerojoueur, app);
        }
        if(scene==3)//Plan de deplacement des bateaux
        {
            deplacerbateau(app,joueur,numerojoueur,bateauDessus, canonCote, scene);
        }
        if(scene==4)//Plan de tir en vue de cote
        {
            tirvuecote(app, joueur, numerojoueur, bateauCote,canonCote,scene);
        }
        if(scene==5)//Plan du tir et de sa trajectoire
        {
            affichageTrajectoire(app,joueur,bateauCote,missile,45, oceanCote, oceanCote2,scene5);
        }

		
		//gestion des joueurs
        if(joueur[numerojoueur].vie<=0)
        {
            suppObj(joueur,numerojoueur);
            if(joueur.size()==1)//fin de la partie + ecran de victoire
            {
                ScreenVictoir(app, joueur , ScreenVictoire , scene,textvictoire);
            }
        }
        else
            numerojoueur++;


        //affichage de la fenetre
        app.display();
        app.setFramerateLimit(60);
    }

    return EXIT_SUCCESS;
}
