#ifndef MATHIS_HPP_INCLUDED
#define MATHIS_HPP_INCLUDED
#include "scriptGestionDonnee.h"
#include "trigo.hpp"
#include <windows.h>
#include "aymeric.hpp"
#include <cmath>
#define PI 3.14159
#define POSITION_JAUGE 10
#define LARGEUR_JAUGE 30



int deplacerbateau(sf::RenderWindow & app, std::vector<Objet> & joueur,int & numerojoueur,sf::Sprite bateau,sf::Sprite canon, int & scene)
{
    int taillejauge = 200;
    bool hautBas = false;

    //initialise l'arrière plan de la jauge de carburant
    sf::RectangleShape fondjauge(sf::Vector2f(taillejauge+10, LARGEUR_JAUGE+10));
    fondjauge.setOrigin(5,5);
    fondjauge.setFillColor(sf::Color(100, 100, 100));
    fondjauge.setPosition(POSITION_JAUGE,POSITION_JAUGE);

    //initialise la jauge de carburant
    sf::RectangleShape jauge(sf::Vector2f(taillejauge, LARGEUR_JAUGE));
    jauge.setOrigin(0,0);
    jauge.setFillColor(sf::Color(0, 255, 0));
    jauge.setPosition(POSITION_JAUGE,POSITION_JAUGE);


    do
    {
        sf::Event event;
        while (app.pollEvent(event))
        {
            // Fermer la fenetre
            if (event.type == sf::Event::Closed)
                app.close();
        }
        //met à jour la position du bateau
        bateau.setPosition(joueur[numerojoueur].x,joueur[numerojoueur].y);
        bateau.setScale(joueur[numerojoueur].taille/100,joueur[numerojoueur].taille/100);
        bateau.setRotation(joueur[numerojoueur].direction);

        //met à jour la position du canon
        float var1 = joueur[numerojoueur].x+125*cos(joueur[numerojoueur].direction*(PI/180));
        float var2 = joueur[numerojoueur].y+125*sin(joueur[numerojoueur].direction*(PI/180));
        canon.setPosition(var1,var2);
        canon.setScale(joueur[numerojoueur].taille/500,joueur[numerojoueur].taille/500);
        sf::Vector2i positionSouris = sf::Mouse::getPosition(app);
        canon.setRotation(calculerAngle(positionSouris.x-var1,var2-positionSouris.y)*180/PI);


        //change la couleur de la jauge de carburant en fonction du carburant restant
        if (taillejauge > 100)
            jauge.setFillColor(sf::Color(0, 255, 0));
        else if (taillejauge > 75 )
            jauge.setFillColor(sf::Color(255, 255, 0));
        else if (taillejauge > 25 )
            jauge.setFillColor(sf::Color(255, 175, 0));
        else
            jauge.setFillColor(sf::Color(255, 0, 0));


        if(taillejauge>0)
        {
            //commandes pour avancer/reculer
            hautBas=(sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down));

            //avancer
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
                taillejauge --;
                avancer(joueur[numerojoueur], 5);

            }

            //reculer
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
                taillejauge --;
                avancer(joueur[numerojoueur], -5);
            }

            //tourner à droite
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && hautBas)
            {
                joueur[numerojoueur].direction +=1;
            }

            //tourner à gauche
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && hautBas)
            {
                joueur[numerojoueur].direction -= 1;
            }
        }

        //valider l'angle de tir et passer à la scène suivante
        if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
        {
            scene++;
        }

        if (taillejauge < 0)
            taillejauge =0;

        //ajuste la taille de la jauge en fonction des commandes réalisées plus tôt
        jauge.setSize(sf::Vector2f(taillejauge, LARGEUR_JAUGE));

        //dessine les différents éléments
        app.clear();
        app.draw(bateau);
        app.draw(canon);
        app.draw(fondjauge);
        app.draw(jauge);
        app.display();

        //limite les images par secondes pour éviter que je jeu se déroule trop rapidement
        app.setFramerateLimit(60);

    }
    while (scene == 3 && app.isOpen());


    //pause de 500 millisecondes pour éviter de passer à la scène suivante trop brusquement
    Sleep(500);
    return  0;
}


#endif // MATHIS_HPP_INCLUDED





