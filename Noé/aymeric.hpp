#ifndef AYMERIC_HPP_INCLUDED
#define AYMERIC_HPP_INCLUDED

#define TAILLE_ANIMATION 1200
#define PUISSANCE 30
#define FENETRE_X 1200
#define FENETRE_Y 800

#include "trigo.hpp"

void animationOcean(sf::RenderWindow & app, sf::Sprite & ocean, int x, int y, int frame)
{
    sf::Rect<int>rectangle(((frame)%11)*TAILLE_ANIMATION,12000-((frame/11)%11)*TAILLE_ANIMATION,TAILLE_ANIMATION,TAILLE_ANIMATION);
    ocean.setTextureRect(rectangle);

    ocean.setPosition(x,y);
    app.draw(ocean);

    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            ocean.setPosition((x%TAILLE_ANIMATION-TAILLE_ANIMATION*(i-1)),(y%TAILLE_ANIMATION-TAILLE_ANIMATION*(j-1)));
            app.draw(ocean);
        }
    }
}

void affichageTrajectoire(sf::RenderWindow & app, std::vector<Objet> & joueur, sf::Sprite bateau, sf::Sprite missile,float direction, sf::Sprite ocean,sf::Sprite ocean2,sf::Sprite ciel)
{
    Objet obj;
    obj.x=0;
    obj.y=200;
    obj.mx=PUISSANCE*cos(direction);
    obj.my=-PUISSANCE*sin(direction);
    obj.taille=15;

    missile.setScale(0.1,0.1);
    bateau.setPosition(-800,650);


    int arrondir;//set juste à convertir les float en int


    while(true)
    {

        app.clear();
        obj.my+=0.5;
        obj.direction=calculerAngle(obj.mx,obj.my)*-180/PI;
        obj.x+=obj.mx;
        obj.y+=obj.my;

        missile.setPosition(app.getSize().x/2,app.getSize().y/2);
        missile.setRotation(obj.direction);
        missile.setScale(obj.taille/100,obj.taille/100);

        bateau.setPosition(-620-obj.x,650-obj.y);

        arrondir=obj.x;


        app.draw(ciel);
        ocean.setPosition(-arrondir%1200,150-obj.y);
        app.draw(ocean);

        ocean2.setPosition(1200-(arrondir%1200),150-obj.y);
        ocean.setPosition(1200-(arrondir%1200),150-obj.y);
        app.draw(ocean);
        app.draw(missile);
        app.draw(bateau);
        app.draw(ocean2);
        app.display();

        app.setFramerateLimit(60);
    }
}

#endif // AYMERIC_HPP_INCLUDED
