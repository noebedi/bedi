#ifndef AYMERIC_HPP_INCLUDED
#define AYMERIC_HPP_INCLUDED

#define TAILLE_OCEAN 1200
#define PUISSANCE 260
#define FENETRE_X 1200
#define FENETRE_Y 800

#include "trigo.hpp"
#include <iostream>

void rendreOcean(sf::RenderWindow & app, sf::Sprite & ocean)
{
    app.draw(ocean);

    for(int i=0; i<10; i++)
    {
        for(int j=0; j<6; j++)
        {
            ocean.setScale(1,1);
            ocean.setPosition((TAILLE_OCEAN*i),(TAILLE_OCEAN*j));
            app.draw(ocean);
        }
    }
}
float distancePlouf(Objet obj)
{
    while(obj.y<350)
    {
        obj.my+=1;
        obj.x+=obj.mx;
        obj.y+=obj.my;
    }
    return obj.x;
}

void affichageTrajectoire(sf::RenderWindow & app, std::vector<Objet> & joueur, sf::Sprite bateau, sf::Sprite missile,float direction, sf::Sprite ocean,sf::Sprite ocean2,sf::Sprite ciel,float direction2, int numJoueur, float & lastX, float & lastY, float canonX, float canonY, int & scene, int & joueurEliminer, std::vector<Objet> & debritParticule1,std::vector<Objet> & debritParticule2, std::vector<Objet> & debritParticule3, bool &sonPlouf, bool &sonExplosion)
{

    Objet obj;
    obj.x=0;
    obj.y=200;
    obj.mx=-PUISSANCE*cos(direction*(PI/180));
    obj.my=PUISSANCE*sin(direction*(PI/180));

    float l = distancePlouf(obj)/10; //on simule la trajectoire afin de savoir où ça vas frapper
    float x = (l*sin(direction2*(PI/180))+canonX);
    float y = (-l*cos(direction2*(PI/180))+canonY);


    bool touche = false;
    int joueurMort;

    for(int i=0; i<joueur.size(); i++)//on vérifie si un joueur sera touché
    {
        if(distance(x,y,joueur[i].x,joueur[i].y)<300)
        {
            touche=true;
            joueurEliminer=joueur[i].id;//il y a deux numero qu'il faut retenir, le premier c'est le numéro associer au joueur, le second est la place dans la liste joueur qui elle peux varier
            joueurMort=i;
        }
    }

    int arrondir;//set juste à convertir les float en int


    while(obj.y<350)
    {
            sf::Event event;
            while (app.pollEvent(event))
            {
                // fermer la fenetre
                if (event.type == sf::Event::Closed)
                    app.close();
            }

        app.clear();
        obj.my+=1;
        obj.direction=calculerAngle(obj.mx,obj.my)*-180/PI;
        obj.x+=obj.mx;
        obj.y+=obj.my;

        missile.setPosition(app.getSize().x/2,app.getSize().y/2);
        missile.setRotation(obj.direction);
        missile.setScale(0.1,0.1);

        bateau.setPosition(-620-obj.x,650-obj.y);

        arrondir=obj.x;


        app.draw(ciel);
        ocean.setPosition(-arrondir%1200,300-obj.y);
        app.draw(ocean);

        ocean2.setPosition(1200-(arrondir%1200),300-obj.y);
        ocean.setPosition(1200-(arrondir%1200),300-obj.y);
        app.draw(ocean);
        app.draw(missile);
        app.draw(bateau);

        if(touche)
        {
            bateau.setPosition(l*10-obj.x,650-obj.y);
            app.draw(bateau);
        }

        app.draw(ocean2);
        app.display();

        app.setFramerateLimit(60);
    }


    if(touche)
    {
        scene=6; //screen coulé
        suppObj(joueur,joueurMort);

        for(int i=0; i<3; i++)
        {
            creeObj(debritParticule1,x,y); //on crée des débrits
            debritParticule1.back().mx=1-(rand()%10)/5;
            debritParticule1.back().my=1-(rand()%10)/5;
            debritParticule1.back().mdirection=1-(rand()%10)/5;
            debritParticule1.back().direction=rand()%360;
            debritParticule1.back().taille=10+rand()%20;

            creeObj(debritParticule2,x,y);
            debritParticule2.back().mx=1-(rand()%10)/5;
            debritParticule2.back().my=1-(rand()%10)/5;
            debritParticule2.back().mdirection=1-(rand()%10)/5;
            debritParticule2.back().direction=rand()%360;
            debritParticule2.back().taille=10+rand()%20;


            creeObj(debritParticule3,x,y);
            debritParticule3.back().mx=1-(rand()%10)/5;
            debritParticule3.back().my=1-(rand()%10)/5;
            debritParticule3.back().mdirection=1-(rand()%10)/5;
            debritParticule3.back().direction=rand()%360;
            debritParticule3.back().taille=10+rand()%20;

            sonExplosion = true;
            sonPlouf = false;

        }
    }
    else
    {
        sonExplosion = false;
        sonPlouf = true;
        scene=2; //premiere scene
    }
    lastX=x; //on renvoie les coordonnées de l'impact pour visualisation
    lastY=y;


}
void creeJoueur(std::vector<Objet> & joueur, int nbre)
{
    joueur.clear();
    for(int i=0; i<nbre; i++)
    {
        creeObj(joueur,rand()%(TAILLE_OCEAN*10),rand()%(TAILLE_OCEAN*6));
        joueur.back().id=i;
    }
}

#endif // AYMERIC_HPP_INCLUDED
