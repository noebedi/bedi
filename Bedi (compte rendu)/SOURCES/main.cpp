#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <time.h>

#include "trigo.hpp"
#include "scriptGestionDonnee.h"
#include "mathis.hpp"
#include "Noe.hpp"
#include "Thomas.hpp"
#include "aymeric.hpp"
#include "ScreenVictoire.h"







using namespace sf;

std::vector<Objet> joueur;
std::vector<Objet> debritParticule1; //débrit qui apparaisse dans deplacementBateau là où il y a eu des naufrages
std::vector<Objet> debritParticule2;
std::vector<Objet> debritParticule3;

int main()
{
    creeJoueur(joueur,2); //on crée 2 joueurs ici, mais ce n'est pas là qu'est gérer la création des joueurs, ici on évite juste un bug

    srand (time(NULL));
    int scene=0, nbJoueur=4, idJoueurCoule;
    float rotationDessus,rotationCote; //variable contenant l'orientation du tir
    float lastX=-500,lastY=-500; // coordonnées du dernier impact pour visualisation
    float canonX, canonY;//coordonné du canon, permet d'ajuster le tir
    bool sonPloufBool = false, sonExplosionBool = false;



    // Create the main window
    RenderWindow app(VideoMode(FENETRE_X, FENETRE_Y), "BATOTEAU");
    app.setFramerateLimit(1);

    //ocean vu de dessus
    Texture textureOcean;
    if (!textureOcean.loadFromFile("Images/ocean.jpg"))
        return EXIT_FAILURE;
    Sprite ocean(textureOcean);

    //debrit qui apparaitrons quand un bateau est coulé
    Texture textureDebrit;
    if (!textureDebrit.loadFromFile("Images/debrit1.png"))
        return EXIT_FAILURE;
    Sprite debrit(textureDebrit);
    Texture textureDebrit2;
    if (!textureDebrit2.loadFromFile("Images/debrit2.png"))
        return EXIT_FAILURE;
    Sprite debrit2(textureDebrit2);
    Texture textureDebrit3;
    if (!textureDebrit3.loadFromFile("Images/debrit3.png"))
        return EXIT_FAILURE;
    Sprite debrit3(textureDebrit3);



    //Charge la texture du bateau vu de cote
    Texture textureBateauCote;
    if (!textureBateauCote.loadFromFile("Images/bateau cote.png"))
        return EXIT_FAILURE;
    Sprite bateauCote(textureBateauCote);
    bateauCote.setOrigin(bateauCote.getTexture()->getSize().x / 2, bateauCote.getTexture()->getSize().y / 2);

    //Charge la texture du bateau vu de dessus
    Texture textureBateauDessus;
    if (!textureBateauDessus.loadFromFile("Images/bateau dessus.png"))
        return EXIT_FAILURE;
    Sprite bateauDessus(textureBateauDessus);
    bateauDessus.setOrigin(bateauDessus.getTexture()->getSize().x / 2, bateauDessus.getTexture()->getSize().y / 2);

    //Charge la texture du canon vu de cote
    Texture textureCanonCote;
    if (!textureCanonCote.loadFromFile("Images/canon cote.png"))
        return EXIT_FAILURE;
    Sprite canonCote(textureCanonCote);
    canonCote.setOrigin(canonCote.getTexture()->getSize().x / 2, canonCote.getTexture()->getSize().y / 2);
    //Charge la texture du cano, vu de dessus
    Texture textureCanonDessus;
    if (!textureCanonDessus.loadFromFile("Images/canon dessus.png"))
        return EXIT_FAILURE;
    Sprite canonDessus(textureCanonDessus);
    canonDessus.setOrigin(canonDessus.getTexture()->getSize().x / 2, canonDessus.getTexture()->getSize().y / 2);

    //Charge la texture du missile
    Texture textureMissile;
    if (!textureMissile.loadFromFile("Images/missile.png"))
        return EXIT_FAILURE;
    Sprite missile(textureMissile);
    missile.setOrigin(missile.getTexture()->getSize().x / 2, missile.getTexture()->getSize().y / 2);

    //textures océans
    Texture textureOceanCote;
    if (!textureOceanCote.loadFromFile("Images/ocean cote.png"))
        return EXIT_FAILURE;
    Sprite oceanCote(textureOceanCote);

    Texture textureOceanCote2;
    if (!textureOceanCote2.loadFromFile("Images/ocean cote2.png"))
        return EXIT_FAILURE;
    Sprite oceanCote2(textureOceanCote2);
    //ciel de la scenes 5
    Texture textureScene5;
    if (!textureScene5.loadFromFile("Images/font scene 5.png"))
        return EXIT_FAILURE;
    Sprite scene5(textureScene5);



    //pour la scene victoire



    Texture NBoutonrejouer;
    if (!NBoutonrejouer.loadFromFile("Images/boutonRejouer.png"))
        return EXIT_FAILURE;
    Sprite Nrejouer(NBoutonrejouer);

    Texture NBoutonmenu;
    if (!NBoutonmenu.loadFromFile("Images/boutonMenu.png"))
        return EXIT_FAILURE;
    Sprite Nmenu(NBoutonmenu);

    Texture NBoutonquitter;
    if (!NBoutonquitter.loadFromFile("Images/boutonquitter.png"))
        return EXIT_FAILURE;
    Sprite Nquitter(NBoutonquitter);



    Font victoire;
    if (!victoire.loadFromFile("Police/arial.ttf"))
    {
        return EXIT_FAILURE;
    }

    //initialise le menu
    Texture textMenu[5];
    Sprite imageMenu[5];
    appliqueTextureMenu(imageMenu, textMenu);
    Texture textOptions[6];
    Sprite imageOptions[6];
    appliqueTextureOptions(imageOptions, textOptions);


    SoundBuffer sonBufferMouette;
    SoundBuffer sonBufferPlouf;
    SoundBuffer sonBufferExplosion;
    SoundBuffer sonBufferTir;
    Sound sonMouette;
    Sound sonPlouf;
    Sound sonExplosion;
    Sound sonTir;
    char fichierSon[4][50] = { "Sons/mouette.wav", "Sons/plouf.wav", "Sons/explosion.wav", "Sons/tir2.wav" };

    if (!sonBufferMouette.loadFromFile(fichierSon[0]))
        return EXIT_FAILURE;
    sonMouette.setBuffer(sonBufferMouette);
    if (!sonBufferPlouf.loadFromFile(fichierSon[1]))
        return EXIT_FAILURE;
    sonPlouf.setBuffer(sonBufferPlouf);
    if (!sonBufferExplosion.loadFromFile(fichierSon[2]))
        return EXIT_FAILURE;
    sonExplosion.setBuffer(sonBufferExplosion);
    if (!sonBufferTir.loadFromFile(fichierSon[3]))
        return EXIT_FAILURE;
    sonTir.setBuffer(sonBufferTir);

    Font font;
    if (!font.loadFromFile("Police/Shiny Signature.ttf"))
    {
        return EXIT_FAILURE;
    }


    int numerojoueur = 0;
    creeObj(joueur,100,100);


    // //Boucle de la fenetre
    while (app.isOpen())
    {
        //sonMouette.play();
        Event event;
        while (app.pollEvent(event))
        {
            // fermer la fenetre
            if (event.type == Event::Closed)
                app.close();
        }

        //deroulement des scenes dans l'ordre

        app.clear();
        sonMouette.setVolume(10);
        sonMouette.play();
        if (scene == -1)//quitter
        {
            app.close();
        }
        if(scene==0)//Menu
        {
            //source dans thomas.hpp
            app.draw(imageMenu[0]);
            menu(FENETRE_X, FENETRE_Y, app, imageMenu);
            gestionMenu(imageMenu, app, scene, nbJoueur, joueur);
        }
        if(scene==1)//Menu
        {
            //source dans thomas.hpp
            app.draw(imageMenu[0]);
            options(FENETRE_X, FENETRE_Y, app, imageOptions, font);
            gestionOptions(imageOptions, app, scene, nbJoueur);
        }
        if(scene==2)//Plan d'ensemble
        {
            //source dans thomas.hpp
            vueGenerale(joueur, numerojoueur, app,bateauDessus, scene, ocean, lastX, lastY);
        }
        if(scene==3)//Plan de deplacement des bateaux
        {
            //source dans mathis.hpp
            deplacerbateau(app,joueur,numerojoueur,bateauDessus, canonDessus, scene,ocean,rotationDessus, canonX, canonY, debrit, debrit2, debrit3, debritParticule1, debritParticule2, debritParticule3);
        }
        if(scene==4)//Plan de tir en vue de cote
        {
            //source dans Noe.hpp
            tirvuecote(app, joueur, numerojoueur, bateauCote,canonCote,scene,rotationCote, scene5,oceanCote,oceanCote2);
            sonTir.play();
        }
        if(scene==5)//Plan du tir et de sa trajectoire
        {
            //source dans aymeric.hpp
            affichageTrajectoire(app,joueur,bateauCote,missile,rotationCote, oceanCote, oceanCote2,scene5,rotationDessus,numerojoueur, lastX, lastY, canonX, canonY,scene,idJoueurCoule, debritParticule1, debritParticule2, debritParticule3, sonPloufBool, sonExplosionBool);

            if (sonPloufBool)
                sonPlouf.play();
            if (sonExplosionBool)
                sonExplosion.play();

        }
        if(scene==6)
        {
            //possibilité d'ajouter le numero du joueur mort
            scene=2;//retour vue general
        }


        //gestion des joueurs
        if(joueur.size()==1)//fin de la partie + ecran de victoire
        {
            //source dans Noe.hpp
            ScreenVictoir(app, joueur, imageMenu, scene, font,Nmenu,Nrejouer,Nquitter, nbJoueur);
            creeJoueur(joueur,nbJoueur);//on recrée des joueur à la fin de la game car sinon on se retrouve tout seul à la prochaine

        }
        else
            numerojoueur++;

        if(numerojoueur>joueur.size()-1)
            numerojoueur=0;


        //affichage de la fenetre
        app.display();
        app.setFramerateLimit(60);
    }

    return EXIT_SUCCESS;
}

