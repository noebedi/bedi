#ifndef TRIGO_HPP_INCLUDED
#define TRIGO_HPP_INCLUDED
#define PI 3.14159
#include "scriptGestionDonnee.h"
#include <cmath>

/*
Le fichier est nommé trigo mais on n'y trouve pas que des fonctions lié à la trigonométrie,
c'est plus les maths en général
*/

typedef struct
{
    float a;
    float b;
}Equation;

void avancer(Objet & vehicle, double vitesse)
{
    vehicle.y+=vitesse*sin(vehicle.direction*(PI/180));
    vehicle.x+=vitesse*cos(vehicle.direction*(PI/180));
}
float distance(int x1, int y1, int x2, int y2)
{
    return(sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)));
}
float distanceEntre(Objet & un, Objet & deux)
{
    return(sqrt((un.x-deux.x)*(un.x-deux.x)+(un.y-deux.y)*(un.y-deux.y)));
}
float calculerAngle(float x, float y)
{
    float retour;


    if(y==0)
        y=0.0001;
    if(x<0 && y <0)
        retour=atan(x/y)-PI;
    else if(x>0 && y <0)
        retour=atan(x/y)+PI;
    else
        retour=atan(x/y);
    return retour;
}
float pointerVers(Objet & un, Objet & deux)
{
    float x = deux.x-un.x;
    float y = un.y-deux.y;

    return calculerAngle(x,y);
}


bool checkCroisement(float x, float y1, float y2, Equation droite)//on vérifie que le segment (x,y1;x,y2) croise la droite d'équation ax+b, si la droite croise l'une des deux hitbox du bateau, alors le bateau est sur la trajectoire du projectile
{
    bool retour = false;
    float y = droite.a*x+droite.b; //ax+b, c'est des fonctions linéaire en Maths
    if((y>y1 && y<y2)||(y>y2 && y<y1))
    {
        retour=true;
    }
    return retour;
}

Equation calculerEquation(float x1, float y1, float x2, float y2)
{
    Equation retour; //on calcule le coef directeur puis la valeur de y à x=0, et on renvoie tout ça
    float x=x2-x1, y=y2-y1;
    if(x==0)
        x=0.0001;
    retour.a=y/x;
    retour.b=-retour.a*x1+y1;

    return retour;
}

#endif // TRIGO_HPP_INCLUDED
